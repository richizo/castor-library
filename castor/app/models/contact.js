import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
  email: DS.attr('string'),
  message: DS.attr('string'),

  validEmail: Ember.computed.match('email', /^.+@.+\..+$/),

  validMessage: Ember.computed.gte('message.length', 5),

  isAllValid: Ember.computed.and('validEmail', 'validMessage'),

  isDisabled: Ember.computed.not('isAllValid')
});
