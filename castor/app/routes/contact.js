import Ember from 'ember';

export default Ember.Route.extend({

  model() {
    return this.store.createRecord('contact');
  },

  actions: {
    saveAction(newContact) {
      newContact.save().then(() => {
        this.controller.set('model.email', '');
        this.controller.set('model.message', '');
        this.controller.set('returnMessage', 'We got your message and we’ll get in touch soon.');
      });
    },

    willTransition() {
      this.controller.get('model').rollbackAttributes();
    }
  }

});
